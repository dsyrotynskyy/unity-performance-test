﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DictionaryTester : BasePerformanceTester
{
    int LENGTH = 1000;
    Dictionary<int, int> _dictionary;
    List<int> _list;

    private void PrepareDictionary()
    {
        _dictionary = new Dictionary<int, int>();
        _list = new List<int>();
        for (int i = 0; i < LENGTH; i++)
        {
            _dictionary.Add(i, i);
            _list.Add(i);
        }
    }

    protected override void PrepareCalculations()
    {
        PrepareDictionary();

        AddMethodToTest(Iterate_Dictionary);
        AddMethodToTest(GetKey_Dictionary);
    }

    private void Iterate_Dictionary()
    {
        var e = _dictionary.GetEnumerator();
        while (e.MoveNext())
        {
            var val = e.Current;
        }
    }

    private void GetKey_Dictionary()
    {
        for (int i = 0; i < LENGTH; i++)
        {
            int val;
            _dictionary.TryGetValue(i, out val);
        }
    }

    private void For_List()
    {
        for (int i = 0; i < _list.Count; i++)
        {
            int j = _list[i];
        }
    }

}
