﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class InstantiateTester : MonoBehaviour {
    public int testCallTimes = 1000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    FooSon f2 = new FooSon ();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>> ();

    public GameObject emptyPrefab;
    public GameObject prefab1Component;
    public GameObject prefab10Components;
    public GameObject prefab1Child;
    public GameObject prefab10Children;
    public GameObject prefab10ChildrenDeep;

    IEnumerator Start () {

        yield return new WaitForSeconds(0.1f);

        Action<Action, string> AddMethodToTest = (Action method, string name) => {
            methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
        };

        //AddMethodToTest(Create_1Child, "Create_1Child");
        //AddMethodToTest(Create_Add1Component, "Create_Add1Component");
        //AddMethodToTest(Prefab_1Component, "Prefab_1Component");
        //AddMethodToTest(Prefab_1Child, "Prefab_1Child");
        //AddMethodToTest(CloneTemplate_1Child, "CloneTemplate_1Child");
        //AddMethodToTest(CloneTemplate_1Component, "CloneTemplate_1Component");

        AddMethodToTest(Create_Empty, "Create_Empty");
        AddMethodToTest(Create_Add10Components, "Create_Add10Components");
        AddMethodToTest(Create_10Children, "Create_10Children");
        AddMethodToTest(Create_10ChildrenDeep, "Create_10ChildrenDeep");

        AddMethodToTest(Prefab_EmptyGo, "Prefab_EmptyGo");
        AddMethodToTest(Prefab_10Components, "Prefab_10Components");
        AddMethodToTest(Prefab_10Children, "Prefab_10Children");
        AddMethodToTest(Prefab_10ChildrenDeep, "Prefab_10ChildrenDeep");

        AddMethodToTest(CloneTemplate_10Children, "CloneTemplate_10Children");
        AddMethodToTest(CloneTemplate_10ChildrenDeep, "CloneTemplate_10ChildrenDeep");
        AddMethodToTest(CloneTemplate_10Components, "CloneTemplate_10Components");

        Debug.Log("Start test ");

        PrepareTemplates();
        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
            yield return new WaitForSeconds(0.1f);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.LogError(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    #region create

    private void Create_1Child()
    {
        new GameObject().transform.parent = (new GameObject().transform);
    }

    private void Create_Empty()
    {
        new GameObject();
    }

    private void Create_Add1Component()
    {
        var g = new GameObject();
        for (int i = 0; i < 1; i++)
        {
            g.AddComponent<TestComponent>();
        }
    }

    private void Create_10Children()
    {
        GameObject parent = new GameObject();
        for (int i = 0; i < 10; i++)
        {
            GameObject child = new GameObject();
            child.transform.parent = parent.transform;
        }
    }

    private void Create_10ChildrenDeep()
    {
        GameObject parent = new GameObject();
        for (int i = 0; i < 10; i++)
        {
            GameObject child = new GameObject();
            child.transform.parent = parent.transform;
            parent = child;
        }
    }

    private void Create_Add10Components()
    {
        var g = new GameObject();
        for (int i = 0; i < 10; i++)
        {
            g.AddComponent<TestComponent>();
        }
    }

    #endregion

    #region prefab
    private void Prefab_EmptyGo()
    {
        var g = GameObject.Instantiate(this.emptyPrefab);
    }

    private void Prefab_1Component()
    {
        var g = GameObject.Instantiate(this.prefab1Component);
    }

    private void Prefab_1Child()
    {
        var g = GameObject.Instantiate(this.prefab1Child);
    }

    private void Prefab_10Components()
    {
        var g = GameObject.Instantiate(this.prefab10Components);
    }

    private void Prefab_10Children()
    {
        var g = GameObject.Instantiate(this.prefab10Children);
    }

    private void Prefab_10ChildrenDeep()
    {
        var g = GameObject.Instantiate(this.prefab10ChildrenDeep);
    }

    #endregion

    #region template

    GameObject _template1Child;
    GameObject _template10Children;
    GameObject _template10ChildrenDeep;
    GameObject _template1Component;
    GameObject _template10Components;

    void PrepareTemplates()
    {
        _template1Child = GameObject.Instantiate(prefab1Child);
        _template10Children = GameObject.Instantiate(prefab10Children);
        _template10ChildrenDeep = GameObject.Instantiate(prefab10ChildrenDeep);
        _template1Component = GameObject.Instantiate(prefab1Component);
        _template10Components = GameObject.Instantiate(prefab10Components);
    }

    private void CloneTemplate_1Child()
    {
        var g = GameObject.Instantiate(this._template1Child);
    }

    private void CloneTemplate_1Component()
    {
        var g = GameObject.Instantiate(this._template1Component);
    }

    private void CloneTemplate_10Components()
    {
        var g = GameObject.Instantiate(this._template10Components);
    }

    private void CloneTemplate_10Children()
    {
        var g = GameObject.Instantiate(this._template10Children);
    }

    private void CloneTemplate_10ChildrenDeep()
    {
        var g = GameObject.Instantiate(this._template10ChildrenDeep);
    }


    #endregion


    void CalculateTime (System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < testCallTimes; i++)
        {
            method();
        }

        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;
        
        if (callDictionary.ContainsKey (methodName))
        {
            callDictionary[methodName] += callTime;
        } else
        {
            callDictionary.Add(methodName, callTime);
        }
    }

    public class Foo
    {
        public virtual void DoSmthVirtual ()
        {

        }

        public void DoDirect ()
        {

        }

        public static void DoSmthStatic() { }
    }

    public class FooSon : Foo
    {
        public override void DoSmthVirtual ()
        {

        }
    }
}
