﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CompareTester : MonoBehaviour {
    public int testCallTimes = 1000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    FooSon f2 = new FooSon ();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>> ();

    IEnumerator Start () {

        yield return new WaitForSeconds(0.1f);

        Action<Action, string> AddMethodToTest = (Action method, string name) => {
            methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
        };

        AddMethodToTest(ReferenceCompareOther, "ReferenceCompareOther");
        AddMethodToTest(ReferenceCompareNull, "ReferenceCompareNull");
        AddMethodToTest(ReferenceCompareSelf, "ReferenceCompareSelf");
        AddMethodToTest(UnityCompareOther, "UnityCompareOther");
        AddMethodToTest(UnityCompareNull, "UnityCompareNull");
        AddMethodToTest(UnityCompareSelf, "UnityCompareSelf");

        Debug.Log("Start test ");

        Prepare();
        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
            yield return new WaitForSeconds(0.1f);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.LogError(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    UnityEngine.Object _testUObject1;
    UnityEngine.Object _testUObject2;
//    Transform _myTransform;

    private void Prepare()
    {
        _testUObject1 = this.gameObject;
        _testUObject2 = new GameObject();
//        _testUObject2 = null;
//        _myTransform = transform;
    }

    #region create

    private void ReferenceCompareNull()
    {
        bool res = System.Object.ReferenceEquals(_testUObject1, null);
    }

    private void ReferenceCompareOther()
    {
        bool res = System.Object.ReferenceEquals(_testUObject1, _testUObject2);
    }

    private void ReferenceCompareSelf()
    {
        bool res = System.Object.ReferenceEquals(_testUObject1, _testUObject1);
    }

    private void UnityCompareSelf()
    {
        bool res = _testUObject1 == _testUObject1;
    }

    private void UnityCompareOther()
    {
        bool res =_testUObject1 == _testUObject2;
    }

    private void UnityCompareNull()
    {
        bool res = _testUObject1 == null;
    }

    //private void TransformByReference()
    //{
    //    System.Object.Equals(_myTransform, null);
    //}

    //private void TransformUnitys()
    //{
    //    System.Object.Equals(transform, null);
    //}

    #endregion



    void CalculateTime (System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < testCallTimes; i++)
        {
            method();
        }

        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;
        
        if (callDictionary.ContainsKey (methodName))
        {
            callDictionary[methodName] += callTime;
        } else
        {
            callDictionary.Add(methodName, callTime);
        }
    }

    public class Foo
    {
        public virtual void DoSmthVirtual ()
        {

        }

        public void DoDirect ()
        {

        }

        public static void DoSmthStatic() { }
    }

    public class FooSon : Foo
    {
        public override void DoSmthVirtual ()
        {

        }
    }
}
