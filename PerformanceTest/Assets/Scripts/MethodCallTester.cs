﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MethodCallTester : MonoBehaviour {
    public int testCallTimes = 1000000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    FooSon f2 = new FooSon ();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>> ();

    void Start () {

        Action<Action, string> AddMethodToTest = (Action method, string name) => {
            methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
        };

        AddMethodToTest(DoSmthNoCall, "Empty for cycle");
        AddMethodToTest(DoSmthStatic, "Static call");
        AddMethodToTest(DoSmthStatic2, "Static call2");
        AddMethodToTest(DoSmthDirect, "Direct call");
        AddMethodToTest(DoSmthDelagate, "Delegate call");
        AddMethodToTest(DoSmthVirtual, "Virtual call");
        AddMethodToTest(DoMathfSqrt2, " Mathf.Sqrt(2f)");
        AddMethodToTest(DoMathSqrt2, " Math.Sqrt(2)");

        Debug.Log("Start test ");

        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.Log(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    private void DoSmthNoCall()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
        }
    }

    void DoSmthDirect()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            f2.DoDirect();
        }
    }

    void DoSmthDelagate()
    {
        System.Action action = f2.DoDirect;

        for (int i = 0; i < testCallTimes; i++)
        {
            action();
        }
    }

    void DoSmthVirtual()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            f2.DoSmthVirtual();
        }
    }

    private void DoSmthStatic()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            Foo.DoSmthStatic();
        }
    }

    private void DoSmthStatic2()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            Foo.DoSmthStatic();
        }
    }

    private void DoMathfSqrt2()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            float f = Mathf.Sqrt(2f);
        }
    }

    private void DoMathSqrt2()
    {
        for (int i = 0; i < testCallTimes; i++)
        {
            double d = Math.Sqrt(2);
        }
    }

    void CalculateTime (System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;
        method();
        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;
        
        if (callDictionary.ContainsKey (methodName))
        {
            callDictionary[methodName] += callTime;
        } else
        {
            callDictionary.Add(methodName, callTime);
        }
    }

    public class Foo
    {
        public virtual void DoSmthVirtual ()
        {

        }

        public void DoDirect ()
        {

        }

        public static void DoSmthStatic() { }
    }

    public class FooSon : Foo
    {
        public override void DoSmthVirtual ()
        {

        }
    }
}
