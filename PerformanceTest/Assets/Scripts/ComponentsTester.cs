﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ComponentsTester : MonoBehaviour {
    public int testCallTimes = 1000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    FooSon f2 = new FooSon ();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>> ();

    IEnumerator Start () {

        yield return new WaitForSeconds(0.1f);

        Action<Action, string> AddMethodToTest = (Action method, string name) => {
            methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
        };

        AddMethodToTest(GetComponentByReference, "GetComponentByReference");
        AddMethodToTest(GetExistingComponentTest, "GetExistingComponentTest");
        AddMethodToTest(GetNonExistingComponentTest, "GetNonExistingComponentTest");

        Debug.Log("Start test ");

        Prepare();
        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
            yield return new WaitForSeconds(0.1f);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.LogError(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    GameObject _victim;
    TestComponent _targetedComponent;

    private void Prepare()
    {
        _victim = new GameObject();
        _targetedComponent = _victim.AddComponent<TestComponent>();
    }

    #region create

    private void GetComponentByReference()
    {
        _targetedComponent.GetHashCode();
    }

    private void GetExistingComponentTest()
    {
        TestComponent t = _victim.GetComponent<TestComponent>();
        t.GetHashCode();
    }

    private void GetNonExistingComponentTest()
    {
        _victim.GetComponent<TestComponent2>();
    }

    #endregion



    void CalculateTime (System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < testCallTimes; i++)
        {
            method();
        }

        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;
        
        if (callDictionary.ContainsKey (methodName))
        {
            callDictionary[methodName] += callTime;
        } else
        {
            callDictionary.Add(methodName, callTime);
        }
    }

    public class Foo
    {
        public virtual void DoSmthVirtual ()
        {

        }

        public void DoDirect ()
        {

        }

        public static void DoSmthStatic() { }
    }

    public class FooSon : Foo
    {
        public override void DoSmthVirtual ()
        {

        }
    }
}
