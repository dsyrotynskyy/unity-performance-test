﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class IterationTester : MonoBehaviour {
    public int testCallTimes = 1000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    FooSon f2 = new FooSon ();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>> ();

    IEnumerator Start () {

        yield return new WaitForSeconds(0.1f);

        Action<Action, string> AddMethodToTest = (Action method, string name) => {
            methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
        };

        AddMethodToTest(For_Array, "For_Array");
        AddMethodToTest(Foreach_Array, "Foreach_Array");
        AddMethodToTest(For_List, "For_List");
        AddMethodToTest(Foreach_List, "Foreach_List");

        Debug.Log("Start test ");
        Debug.Log("Collection length = " + LENGTH);

        Prepare();
        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
            yield return new WaitForSeconds(0.1f);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.LogError(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    int LENGTH = 1000;
    int[] _array;
    List<int> _list;

    private void Prepare()
    {
        _list = new List<int>(LENGTH);
        for (int i = 0; i < LENGTH; i++)
        {
            _list.Add(i + 1);
        }
        _array = _list.ToArray();
    }

    #region create

    private void For_Array()
    {
        for (int i = 0; i < _array.Length; i++)
        {
            int j = _array[i];
        }
    }

    private void For_List()
    {
        for (int i = 0; i < _list.Count; i++)
        {
            int j = _list[i];
        }
    }

    private void Foreach_Array()
    {
        foreach (int i in _array)
        {
            int j = i;
        }
    }

    private void Foreach_List()
    {
        foreach (int i in _list)
        {
            int j = i;
        }
    }

    #endregion

    void CalculateTime (System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < testCallTimes; i++)
        {
            method();
        }

        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;
        
        if (callDictionary.ContainsKey (methodName))
        {
            callDictionary[methodName] += callTime;
        } else
        {
            callDictionary.Add(methodName, callTime);
        }
    }

    public class Foo
    {
        public virtual void DoSmthVirtual ()
        {

        }

        public void DoDirect ()
        {

        }

        public static void DoSmthStatic() { }
    }

    public class FooSon : Foo
    {
        public override void DoSmthVirtual ()
        {

        }
    }
}
