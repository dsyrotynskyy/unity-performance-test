﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class BasePerformanceTester : MonoBehaviour
{
    public int testCallTimes = 1000;
    Dictionary<string, float> callDictionary = new Dictionary<string, float>();
    List<KeyValuePair<Action, string>> methodsToTest = new List<KeyValuePair<Action, string>>();

    protected void AddMethodToTest(Action method)
    {
        AddMethodToTest(method, method.Method.Name);
    }

    protected void AddMethodToTest (Action method, string name)
    {
        methodsToTest.Add(new KeyValuePair<Action, string>(method, name));
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);

        PrepareCalculations();

        Debug.Log("Start test ");

        foreach (var v in methodsToTest)
        {
            CalculateTime(v.Key, v.Value);
            yield return new WaitForSeconds(0.1f);
        }

        Debug.Log("Total iteration count = " + testCallTimes);
        var sortedDict = from entry in callDictionary orderby entry.Value ascending select entry;
        foreach (var v in sortedDict)
        {
            Debug.LogError(string.Format("Time of '{0}' = {1}", v.Key, v.Value));
        }
    }

    protected abstract void PrepareCalculations();

    void CalculateTime(System.Action method, string methodName)
    {
        float startTime = Time.realtimeSinceStartup;

        for (int i = 0; i < testCallTimes; i++)
        {
            method();
        }

        float endTime = Time.realtimeSinceStartup;
        float callTime = endTime - startTime;

        if (callDictionary.ContainsKey(methodName))
        {
            callDictionary[methodName] += callTime;
        }
        else
        {
            callDictionary.Add(methodName, callTime);
        }
    }
}

